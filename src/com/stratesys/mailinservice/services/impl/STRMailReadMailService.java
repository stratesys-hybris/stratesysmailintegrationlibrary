package com.stratesys.mailinservice.services.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeUtility;

import org.apache.log4j.Logger;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.stratesys.mailingservice.data.STRMailingAttachmentsData;
import com.stratesys.mailingsevice.data.response.STRMailingResponseData;

public class STRMailReadMailService {

	STRMailingResponseData responseData = null;
	private String mediaFolderName = null;
	private static final Logger LOGGER = Logger.getLogger( STRMailReadMailService.class.getName() );



	public STRMailReadMailService (STRMailingResponseData responseData){
		this.responseData = responseData;
		//Name of folder
		mediaFolderName = new File("").getAbsolutePath().replaceAll("\\\\", "/") + "/" + responseData.getRemitente().substring(0, responseData.getRemitente().indexOf("@")) + new Date().getTime();
		mediaFolderName = mediaFolderName.replaceAll(" ", "");
		new File(mediaFolderName).mkdirs();
	}


	public String readMediaElement(Part part) throws Exception {
		String fileFullName = null;
		if (part.isMimeType("image/*") || part.isMimeType("application/*") ||
				(part.isMimeType("text/plain") && part.getFileName() != null))
		{
			LOGGER.info("Starting image processing part");
			FileOutputStream fichero = null;
			try{
				String filename = "";
				try {	
					filename =  MimeUtility.decodeText(part.getFileName().replaceAll(" ", ""));
				}
				catch (Exception e){
					LOGGER.error("error decoding filename text, so using original name " + part.getFileName() );
					LOGGER.info("trying to rename it");
					try{
						filename =  part.getFileName().replaceAll(" ", "");
					}catch (Exception e2){
						LOGGER.error("Also failed using original filename , so using a generic one");
						filename = "adjunto" + new Date().getTime();
					}
				}
				//Comportamiento extra�o que provoca caracteres extra�os al final de la extension
				if (filename.endsWith("?="))
					filename = filename.substring(0,filename.length()-2);
				fichero = new FileOutputStream("/" + mediaFolderName + "/" + MimeUtility.decodeText(filename));
				InputStream media = part.getInputStream();
				byte [] bytes = new byte[1000];
				int leidos=0;
				while ((leidos=media.read(bytes))>0)
				{
					fichero.write(bytes,0,leidos);
				}
				fileFullName =  filename;
				LOGGER.info("Finished image processing part " + fileFullName);
			}catch (Exception e){
				LOGGER.error("Error  procesing media attachment: ",e);
			}
			finally{
				fichero.close();
			}
		}
		return fileFullName;
	}

	public void readMultiPart(Part part) throws Exception{


		// Volvemos a analizar cada parte de la MultiParte
		if (part.isMimeType ("multipart/*")){
			LOGGER.info("There is a multipart inside a multipart");
			// Obtenemos el contenido, que es de tipo MultiPart.
			Multipart multi = (Multipart)part.getContent();

			// Extraemos cada una de las partes.
			for (int j=0;j<multi.getCount();j++)
			{
				LOGGER.info("Reading part of multipart inside a multipart number " + j);
				Part onePart = multi.getBodyPart(j);
				readMultiPart(onePart);
			}
		}
		else
			if (part.isMimeType("image/*") || part.isMimeType("application/*") ||
					(part.isMimeType("text/plain") && part.getFileName() != null)){

				LOGGER.info("There is an media part, processing");
				String fullNameFile = readMediaElement(part);
				String fileName = fullNameFile;
				fullNameFile = mediaFolderName + "/" + fullNameFile;
				
				List<STRMailingAttachmentsData> newList = new ArrayList();	
				if (responseData.getFicherosAdjuntos() != null)
					newList.addAll(responseData.getFicherosAdjuntos());

				STRMailingAttachmentsData attachmentData = new STRMailingAttachmentsData();
				attachmentData.setFullPath(fullNameFile);
				attachmentData.setFileName(fileName);
				attachmentData.setFolderName(mediaFolderName);
				
				File file = null;
				LOGGER.info("Storing attachment " + fullNameFile);
				try
				{
					file = new File(fullNameFile);  

					Path path = Paths.get(fullNameFile);
					byte[] data = Files.readAllBytes(path);
					String contentType = "";

					if (part.getContentType().contains("vnd.openxmlformats-officedocument.wordprocessingml")){
						LOGGER.info("Its a word attachmentt");
						contentType = "application/msword";
					}
					else
						if (part.getContentType().contains("pvnd.openxmlformats-officedocument.spreadsheetml")){
							LOGGER.info("Its a excel attachment");
							contentType = "application/vnd.ms-excel";
						}
						else{
							LOGGER.info("Its not a word or excel attachment");
							contentType = part.getContentType();
						}
					
					MultipartFile multipartFile = new MockMultipartFile(attachmentData.getFileName(),
							file.getName(), contentType, data);
					attachmentData.setMultipart(multipartFile);
					LOGGER.info("Finish Storing attachment " + fullNameFile);

				}
				catch (Exception e1)
				{ 
					LOGGER.error("Error creating attachment", e1);
				}				
				newList.add(attachmentData);
				this.responseData.setFicherosAdjuntos(newList);
			}
			else
				if (part.isMimeType("text/plain") && (responseData.getMensaje() == null || ((String)responseData.getMensaje()).trim().isEmpty())){
					this.responseData.setMensaje((String) part.getContent());
				}
				else
					if (part.isMimeType("text/html")){
						if (responseData.getMensaje() == null)
							responseData.setMensaje("");
						this.responseData.setMensaje(responseData.getMensaje().concat((String) part.getContent()));
					}
	}

}

