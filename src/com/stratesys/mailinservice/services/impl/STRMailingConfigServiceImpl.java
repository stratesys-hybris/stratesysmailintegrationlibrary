package com.stratesys.mailinservice.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.search.MessageIDTerm;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.stratesys.mailingservice.constants.STRMailingConstants;
import com.stratesys.mailingservice.data.STRMailingAllDataConfiguration;
import com.stratesys.mailingservice.data.STRMailingAttachmentsData;
import com.stratesys.mailingservice.data.STRMailingConfigurationData;
import com.stratesys.mailingservice.data.STRMailingConnectionCredentials;
import com.stratesys.mailingservice.data.connection.STRMailingConnectionRequest;
import com.stratesys.mailingservice.data.mailaccount.STRMailingAccountConfigurationData;
import com.stratesys.mailingservice.data.mailaccount.STRMailingGoogleAccountIMAPConfigurationData;
import com.stratesys.mailingservice.data.mailaccount.STRMailingGoogleAccountSMTPConfigurationData;
import com.stratesys.mailingservice.data.mailaccount.STRMailingHDREstrellaAccountIMAPConfigurationData;
import com.stratesys.mailingservice.data.mailaccount.STRMailingHDREstrellaAccountSMTPConfigurationData;
import com.stratesys.mailingservice.data.request.STRMailingRequestData;
import com.stratesys.mailingsevice.data.response.STRMailingResponseData;
import com.stratesys.malingservice.services.STRMailingConfigService;
import com.sun.mail.imap.IMAPFolder;

public class STRMailingConfigServiceImpl implements STRMailingConfigService{



	public STRMailingConfigServiceImpl(org.apache.log4j.Level level, String log4jConfPath) {
		//String log4jConfPath = "./lib/log4j.properties";
		PropertyConfigurator.configure(log4jConfPath);
		LOGGER.getRootLogger().setLevel(level);
	}
	
	public STRMailingConfigServiceImpl(){
		LOGGER.getRootLogger().setLevel(Level.INFO);
	}
	private static final Logger LOGGER = Logger.getLogger( STRMailingConfigServiceImpl.class );
	@Override
	public STRMailingConfigurationData initializeMailConfigurationData(STRMailingAccountConfigurationData accountConfigurationData, STRMailingConnectionCredentials credentials) throws Exception{	

		if (accountConfigurationData == null)
			throw new Exception("Error with configuration Data, account configuration parameter cannot be blank");

		if (credentials == null)
			throw new Exception("Error with configuration Data, credentials cannot be blank");

		STRMailingConfigurationData configurationData = new STRMailingConfigurationData(accountConfigurationData, credentials);
		return configurationData;
	}

	@Override
	public Session connectToMailServer(STRMailingConfigurationData configurationData,
			STRMailingConnectionRequest request) throws Exception {


		if (configurationData == null)
			throw new Exception("Error with configuration Data, configurationData parameter cannot be blank");

		if (request == null)
			throw new Exception("Error with configuration Data, request parameter cannot be blank");


		Properties prop = configurationData.getMailAccountConfiguration().getPropertiesConfiguration();
		Session sesion = Session.getInstance(prop);
		sesion.setDebug(request.isDevelopmentMode());


		return sesion;

	}

	@Override
	public Folder openMailFolder(STRMailingConfigurationData configurationData, STRMailingConnectionRequest request, Session session) throws MessagingException{

		URLName urlName = new URLName(configurationData.getMailAccountConfiguration().getStoreCode(),
				configurationData.getMailAccountConfiguration().getHost(), 
				Integer.valueOf(configurationData.getMailAccountConfiguration().getPort()).intValue(),
				null,
				configurationData.getCredentials().getFolderAlias() == null ?
						configurationData.getCredentials().getUser() : 
							configurationData.getMailAccountConfiguration().getUser() + "\\" + configurationData.getCredentials().getFolderAlias(),
							configurationData.getCredentials().getPass());
		Store store = session.getStore(urlName);
		LOGGER.info("Init connection to imap folder");
		store.connect();
		LOGGER.info("Succesfully connected to imap folder");
		Folder folder = store.getFolder(request.getFolderName());
		folder.open(request.getConnectionMode());

		return folder;
	}

	private Folder getMessageIMAPFolder(STRMailingAccountConfigurationData configurationData, STRMailingConnectionCredentials credentials , String folderName) throws Exception{
		STRMailingConfigurationData configData = initializeMailConfigurationData(configurationData, credentials);

		//First prepare request
		Folder folder = null;
		try
		{
			LOGGER.info("Getting imap folder to read mails");
			STRMailingConnectionRequest request = new STRMailingConnectionRequest(false, folderName, Folder.READ_WRITE);
			Session session = connectToMailServer(configData, request);
			folder = openMailFolder(configData, request, session);
			LOGGER.info("folder already opened");
			//si se ha abierto correctamente la conexi�n
			if (folder != null && folder.isOpen()){
				LOGGER.info("folder correctly opened");
				return folder;
			}
			else
				LOGGER.error("Error opening folder, was not correctly opened");
		}
		catch (Exception e){
			LOGGER.error("Error trying to   search for a previouse mails  ", e);
			if (folder != null && folder.isOpen())
				folder.close(true);
			throw e;
		}


		return null;
	}
	@Override
	public List<STRMailingResponseData> readNewReceivedMails(STRMailingConfigurationData configurationData, List<String> whiteList) throws Exception {

		List<STRMailingResponseData> mailsList = new ArrayList();
		//First prepare request
		IMAPFolder folder = null;
		IMAPFolder imapFolfer = null;
		Folder processedFolder = null;
		try
		{				
			LOGGER.info("Starting proccess to read mails of folder " + configurationData.getCredentials().getFolderAlias());
			folder = (IMAPFolder) getMessageIMAPFolder(configurationData.getMailAccountConfiguration(), configurationData.getCredentials(), STRMailingConstants.FOLDER_INBOX);

			//si se ha abierto correctamente la conexi�n y hay mensajes 
			if (folder != null && folder.isOpen()){
				LOGGER.info("The folder is openend");
				//Abrimos la 
				processedFolder = createFolder(folder, STRMailingConstants.PROCESSED_FOLDER_NAME);
				Message[] mails = folder.getMessages();
				Message[] messagesToMove = new Message[mails.length];
				int messagesToMoveCount = 0;
				
				LOGGER.info("Total number of mails to process : " +mails.length);
				for (int i = 0 ; i <mails.length ; i++){
					try 
					{
						Message mail = mails[i];
						LOGGER.info("Reading mail from " + ((InternetAddress)mail.getFrom()[0]).getAddress() + " and subject " + mail.getSubject());
						if (mail.getFlags().contains(Flags.Flag.SEEN))
						{
							LOGGER.info("Mail already processed, so added to move, SUBJECT: " + mail.getSubject());
							messagesToMove[messagesToMoveCount] =mail;
							messagesToMoveCount++;
						}
						else
						{
							STRMailingResponseData responseData = new STRMailingResponseData();
							if (mail.getSubject() != null && !mail.getSubject().trim().isEmpty()) {
								try {
									responseData.setAsunto(MimeUtility.decodeText(mail.getSubject()));
								}
								catch (Exception e){
									LOGGER.info("error decoding subject : " + mail.getSubject());
								}
							}
							else
								responseData.setAsunto(" ");

							responseData.setFecha(mail.getSentDate());
							responseData.setRemitente(((InternetAddress)mail.getFrom()[0]).getAddress());
							responseData.setMailReceivedFrom(configurationData.getCredentials().getUser());

							Enumeration<Header> headers = mail.getAllHeaders();
							LOGGER.info("looking for headers and references of mail " + mail.getSubject());
							while(headers.hasMoreElements()){
								Header header = headers.nextElement();
								if (header.getName().equals(STRMailingConstants.HEADER_MESSAGE_ID)){
									LOGGER.info("Found HEADER_MESSAGE_ID :" + header.getValue());
									responseData.setIdThread(header.getValue());
								}
								else
									if (header.getName().equals("References")){
										try {
											LOGGER.info("Found Reference message " + header.getValue());
											if (header.getValue().contains("<") && header.getValue().contains(">")){
												String originalId =header.getValue().substring(header.getValue().indexOf("<"),
														header.getValue().indexOf(">") + 1);
												responseData.setOriginalThreadId(originalId);
												LOGGER.info("Assigned original thread id " + originalId);
											}
											else
												LOGGER.error("Reference message found but now correctly formed");
										}
										catch (Exception e){
											LOGGER.error("Error reading reference "+ header.getValue(), e );
										}
									}
							}
							LOGGER.info("Finished looking for headers and references of mail " + mail.getSubject());

							// mensaje de texto simple
							LOGGER.info("Reading mail body");
							if (mail.isMimeType("text/*")){
								LOGGER.info("Mail has simple text body");
								responseData.setMensaje((String) mail.getContent());
							}
							else
							{				
								if (mail.isMimeType("application/pdf") ||
										mail.isMimeType("application/*")){
									LOGGER.info("Is a empty mail with a single attachment, Starting processing mail multipart ");
									STRMailReadMailService readMailService = new STRMailReadMailService(responseData);
									readMailService.readMultiPart(mail);
									LOGGER.info("Is a empty mail with a single attachment, Finished processing mail multipart ");
									
								}
								else
								// mensaje compuesto
								if (mail.isMimeType("multipart/*")){
									LOGGER.info("Starting processing mail multipart ");
									Multipart multi = (Multipart)mail.getContent();

									// Extraemos cada una de las partes.
									for (int j=0;j<multi.getCount();j++)
									{
										LOGGER.info("Extracting multipart number " + j);
										STRMailReadMailService readMailService = new STRMailReadMailService(responseData);
										readMailService.readMultiPart(multi.getBodyPart(j));
									}
									LOGGER.info(" Finish processing mail multipart");
								}

							}
							postProcessingMail(responseData,whiteList);
							mailsList.add(responseData);
							LOGGER.debug("Finished reading mail");
							folder.setFlags(new Message[] {mail}, new Flags(Flags.Flag.SEEN), true);
							messagesToMove[messagesToMoveCount] =mail;
							messagesToMoveCount++;;
						}


					}
					catch (Exception e){
						LOGGER.error("Error reading concrete mail, continuing with other ", e);
					}
				}
				
				if (messagesToMove != null && messagesToMove.length > 0){
				
					LOGGER.info("Moving processed mail to processed folder");
					//The seen messages are directly moved
					folder.moveMessages(messagesToMove, processedFolder);
					LOGGER.info("Finished moving processed mail to processed folder");
				}
			}
			else
				LOGGER.debug("Theres no new mails");
		}
		catch (Exception e){
			LOGGER.error("Error trying to read mails ", e);
			throw e;
		}
		finally {
			if (folder != null && folder.isOpen())
				folder.close(true);

			if (imapFolfer != null && imapFolfer.isOpen())
				imapFolfer.close(true);

			if (processedFolder != null && processedFolder.isOpen())
				processedFolder.close(true);
		}
		LOGGER.info("Finished mail reading process for folder " + configurationData.getCredentials().getFolderAlias());
				
		return mailsList;	
	}

	private void postProcessingMail(STRMailingResponseData responseData, List<String> whiteList) {
		if (responseData.getMensaje() != null && !responseData.getMensaje().isEmpty()){
			
			String message = responseData.getMensaje();
			boolean allowLinks = false;
			if(whiteList != null && !whiteList.isEmpty() && responseData.getRemitente() != null)
			{
				for(String whiteElement : whiteList)
				{
					if(responseData.getRemitente().contains(whiteElement))
					{
						allowLinks = true;
						break;
					}
				}
			}
			if(!allowLinks)
			{
				message = message.replaceAll("href", "");
			}
			
			message = message.replaceAll("file:", "");
			message = message.replaceAll("<script language=\"javascript\">", "");
			message = message.replaceAll("</script>", "");
			
			responseData.setMensaje(message);
		}
	}

	@Override
	public void closeConnection(List<STRMailingResponseData> dataList) throws Exception {
		// TODO Auto-generated method stub
		LOGGER.info("deleting attachment temporaly files");
		for (STRMailingResponseData data :dataList){
			if (data.getFicherosAdjuntos() != null && data.getFicherosAdjuntos().size() > 0)
				for (STRMailingAttachmentsData attachment : data.getFicherosAdjuntos()){

					FileUtils.deleteDirectory(new File(attachment.getFolderName()));
				}
		}
		LOGGER.info("finished deleting attachment temporaly files");
	}

	@Override
	public List<String> writeNewListEmails(STRMailingConfigurationData configurationData,
			List<STRMailingRequestData> requestDataList, int timeout) throws Exception {
			STRMailingAllDataConfiguration result = null;
			List<String> messageIdList = new ArrayList<>();
		try {
			LOGGER.info("init sending all mails");
			LOGGER.info("getting configuration");
			
			ExecutorService executor = Executors.newCachedThreadPool();
			Callable<STRMailingAllDataConfiguration> task = new Callable<STRMailingAllDataConfiguration>() {
			   public STRMailingAllDataConfiguration call() throws Exception {
				   STRMailingAllDataConfiguration config = new STRMailingAllDataConfiguration();
			      
				   config.setSession(getSession(configurationData));
				   config.setImapFolder(getImapFolder(configurationData));
				   config.setTransport(createTransportConnection(config.getSession(), configurationData));
				   
				   return config;
			   }
			};
			Future<STRMailingAllDataConfiguration> future = executor.submit(task);
			
			try {
				if ( timeout <= 0)
					timeout = 30;
				
			    result = future.get(timeout, TimeUnit.SECONDS); 
			} catch (TimeoutException ex) {
			   LOGGER.error("Getting configuration returned timeout, so aborting this execution and waiting for next cycle");
			   throw ex;
			} catch (InterruptedException ex) {
				LOGGER.error("Getting configuration was interrupted, so aborting this execution and waiting for next cycle");
				throw ex;
			} catch (ExecutionException ex) {
				LOGGER.error("Getting configuration returned uncontrolled error, so aborting this execution and waiting for next cycle");
				throw ex;
			} catch (Exception ex) {
				LOGGER.error("Getting configuration returned uncontrolled error, so aborting this execution and waiting for next cycle");
				throw ex;
			}
			 finally {
			   future.cancel(true); // may or may not desire this
			}
			
			LOGGER.info("finished geting configuration");
			if (result != null && result.getImapFolder() != null && 
					result.getSession() != null && result.getTransport() != null)
			
			for (STRMailingRequestData requestData: requestDataList){
				try{
					String messageId = writeNewEmail(configurationData, requestData, result.getSession(),
							result.getImapFolder(), result.getTransport());
					messageIdList.add(messageId);
				}
				catch (Exception e){
					LOGGER.error("Error sending mail with subject " + requestData.getAsunto() + ", will try again in next execution", e);
					messageIdList.add(null);
				}
			}
		}
		catch (Exception e){
			LOGGER.error("error sending mails, ",e);
		}
		finally {
			if (result != null){
			if (result.getImapFolder() != null && result.getImapFolder().isOpen())
				result.getImapFolder().close();
			if ( result.getTransport() != null &&  result.getTransport().isConnected())
				 result.getTransport().close();
			}
		}
		return messageIdList;
	}
		
	private Folder getImapFolder(STRMailingConfigurationData configurationData) throws Exception{
		Folder imapFolfer = null;
		LOGGER.info("Getting imap Folder");
		
		if (configurationData.getMailAccountConfiguration() instanceof STRMailingGoogleAccountSMTPConfigurationData) {
			imapFolfer = getMessageIMAPFolder(new STRMailingGoogleAccountIMAPConfigurationData(), configurationData.getCredentials(),STRMailingConstants.FOLDER_INBOX + "/" +  STRMailingConstants.PROCESSED_FOLDER_NAME);
		}
		else if (configurationData.getMailAccountConfiguration() instanceof STRMailingHDREstrellaAccountSMTPConfigurationData) {
			imapFolfer = getMessageIMAPFolder(new STRMailingHDREstrellaAccountIMAPConfigurationData(configurationData.getMailAccountConfiguration().getMailSmtpUser()), configurationData.getCredentials(), STRMailingConstants.FOLDER_INBOX + "/" +  STRMailingConstants.PROCESSED_FOLDER_NAME);
		}

		return imapFolfer;
	}
	
	private Session getSession(STRMailingConfigurationData configurationData) throws Exception{
		STRMailingConnectionRequest request = new STRMailingConnectionRequest(false, STRMailingConstants.FOLDER_INBOX, Folder.READ_WRITE);
		LOGGER.info("Connecting to mail server");
		Session session = connectToMailServer(configurationData, request);
		
		return session;

	}
	
	private Transport createTransportConnection(Session session, STRMailingConfigurationData configurationData) throws NumberFormatException, MessagingException{
		LOGGER.info("Init transport connection");
		Transport transport = session.getTransport("smtp");
		transport.connect(configurationData.getMailAccountConfiguration().getHost(),
				Integer.parseInt(configurationData.getMailAccountConfiguration().getMailSmtpPort()),
				configurationData.getMailAccountConfiguration().getMailSmtpUser(),
				configurationData.getCredentials().getPass());
		LOGGER.info("Finished transport connection");
	
		
		return transport;
	}
	
	@Override
	public String writeNewEmail(STRMailingConfigurationData configurationData,
			STRMailingRequestData requestData, Session session, Folder imapFolder, Transport transport) throws Exception {

		String messageId = "";
		try
		{		
			LOGGER.info("Starting process to write an email");
			LOGGER.info("FROM MAIL " + requestData.getFromMail());
			if (requestData.getCommercialMail() != null && !requestData.getCommercialMail().isEmpty())
				LOGGER.info("TO COMMERCIAL NAME MAIL " + requestData.getCommercialMail());
			else
				LOGGER.info("TO USER MAIL " + requestData.getToMail());
			LOGGER.info("SUBJECT " + requestData.getAsunto());
			LOGGER.info(" TO CC: " + requestData.getCc());
		

			MimeMessage message = null;
			Message replyMessage = null;

			if (requestData.getReplyMessageId() != null && !requestData.getReplyMessageId().isEmpty()){
				LOGGER.info("Looking for thread mail id in order to respond it, thread id " + requestData.getReplyMessageId());
				
				Message[] messagesSearched = imapFolder.search(new MessageIDTerm(requestData.getReplyMessageId()));//not found
				if (messagesSearched != null && messagesSearched.length > 0){
					LOGGER.info("Found thread : " + requestData.getReplyMessageId());
					replyMessage =  messagesSearched[0];
				}
				else
					LOGGER.error("Not found thread " + requestData.getReplyMessageId());
			}
			LOGGER.info("Preparing body mail");
			BodyPart messageBodyPart = null;
			message = new MimeMessage(session);
			message.setFrom(new InternetAddress(requestData.getFromMail()));
			Address[] addresses = new Address[1];
			addresses[0] = new InternetAddress(requestData.getFromMail());
			message.setReplyTo(addresses);
			message.setSubject(requestData.getAsunto());
			if (requestData.getCommercialMail() != null && !requestData.getCommercialMail().isEmpty())
				message.setRecipients(Message.RecipientType.TO, requestData.getCommercialMail());
			else	
			message.setRecipients(Message.RecipientType.TO, requestData.getToMail());

			if (requestData.getCc() != null && !requestData.getCc().isEmpty())
				message.setRecipients(Message.RecipientType.CC, requestData.getCc());
			
			String text = requestData.getMensaje();
			MimeBodyPart messageBodyPart2 = null;
			if (replyMessage != null){

				messageBodyPart2 = new MimeBodyPart();  
				messageBodyPart2.setDataHandler(replyMessage.getDataHandler());  

				message.setHeader("In-Reply-To", requestData.getReplyMessageId());
				message.setSubject("RE:" + replyMessage.getSubject() != null ? replyMessage.getSubject() : "");
			}

			message.setContent(text, "text/html; charset=utf-8");


			// Create your new message part  
			messageBodyPart = new MimeBodyPart();  
			//messageBodyPart.setText(text);  
			messageBodyPart.setContent(text, "text/html");
			// Create a multi-part to combine the parts  
			Multipart multipart = new MimeMultipart();  

			// Add part to multi part  
			if (messageBodyPart != null){
				multipart.addBodyPart(messageBodyPart);
			}

			if (requestData.getFicherosAdjuntos() != null){
				LOGGER.info("Attaching attachments");
				for (STRMailingAttachmentsData attachment : requestData.getFicherosAdjuntos()){
					try
					{
						LOGGER.info("Init Attaching attachment " + attachment.getFileName());
						BodyPart adjunto = new MimeBodyPart();
						adjunto.setDataHandler(new DataHandler(new FileDataSource(attachment.getFullPath())));
						adjunto.setFileName(attachment.getFileName());
						multipart.addBodyPart(adjunto);
						LOGGER.info("Finish Attaching attachment " + attachment.getFileName());
					}
					catch (Exception e){
						LOGGER.error("error attaching attachment", e);
					}
				}
			}
			// Associate multi-part with message  
			message.setContent(multipart);  

			LOGGER.info("Init sending message");
			transport.sendMessage(message,message.getAllRecipients());
			LOGGER.info("Message sent succesfully");
			messageId = message.getMessageID();
			LOGGER.debug("Finished process to write an email");

		}
		catch (Exception e){
			LOGGER.error("Error trying to write mails ", e);
			throw e;
		}
		return messageId;
	}



	private Folder createFolder(Folder parent, String name)
			throws Exception {
		Folder newFolder = null;
		boolean isCreated = true;   

		newFolder = parent.getFolder(name);   
		isCreated = newFolder.create(Folder.HOLDS_MESSAGES);   

		return newFolder;
	}

}
