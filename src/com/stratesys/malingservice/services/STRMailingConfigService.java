package com.stratesys.malingservice.services;

import java.util.List;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;

import com.stratesys.mailingservice.data.STRMailingConfigurationData;
import com.stratesys.mailingservice.data.STRMailingConnectionCredentials;
import com.stratesys.mailingservice.data.connection.STRMailingConnectionRequest;
import com.stratesys.mailingservice.data.mailaccount.STRMailingAccountConfigurationData;
import com.stratesys.mailingservice.data.request.STRMailingRequestData;
import com.stratesys.mailingsevice.data.response.STRMailingResponseData;

public interface STRMailingConfigService {

	public STRMailingConfigurationData initializeMailConfigurationData(STRMailingAccountConfigurationData accountConfigurationData, STRMailingConnectionCredentials credentials) throws Exception;
	
	public Session connectToMailServer(STRMailingConfigurationData configurationData, STRMailingConnectionRequest request) throws Exception;
	
	public void closeConnection(List<STRMailingResponseData> dataList) throws Exception;
	
	List<STRMailingResponseData> readNewReceivedMails(STRMailingConfigurationData configurationData, List<String> whiteList) throws Exception;
	
	public Folder openMailFolder(STRMailingConfigurationData configurationData, STRMailingConnectionRequest request, Session session) throws MessagingException;

	
	String writeNewEmail(STRMailingConfigurationData configurationData, STRMailingRequestData requestData,
			Session session, Folder imapFolder, Transport transport) throws Exception;

	List<String> writeNewListEmails(STRMailingConfigurationData configurationData,
			List<STRMailingRequestData> requestDataList, int timeout) throws Exception;
	
}
