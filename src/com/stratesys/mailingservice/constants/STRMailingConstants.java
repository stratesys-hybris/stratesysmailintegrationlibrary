package com.stratesys.mailingservice.constants;

public class STRMailingConstants {

	public static final  String FOLDER_INBOX = "INBOX";
	public final static String PROCESSED_FOLDER_NAME = "Processed_Mails";
	public static final String HEADER_MESSAGE_ID = "Message-ID";
}
