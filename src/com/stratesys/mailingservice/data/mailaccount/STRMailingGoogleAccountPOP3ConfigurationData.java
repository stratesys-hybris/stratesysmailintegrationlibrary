package com.stratesys.mailingservice.data.mailaccount;

import java.util.Properties;

/**
 * 
 * @author Diego.
 *
 */
public class STRMailingGoogleAccountPOP3ConfigurationData extends STRMailingAccountConfigurationData{


	private static final String GOOGLE_SSL = "javax.net.ssl.SSLSocketFactory";
	private static final String GOOGLE_POP3_PORT = "995"; 


	public STRMailingGoogleAccountPOP3ConfigurationData() throws Exception{
		
		setStartTLS(false);
		setHost("pop.gmail.com");
		setUseSSL(GOOGLE_SSL);
		setUseSocketFactoryFallback(false);
		//Para google, por defecto 995
		setPop3Port(GOOGLE_POP3_PORT);
		setPop3SocketFactory(GOOGLE_POP3_PORT);
		setStoreCode("pop3");
	}

	@Override
	public Properties getPropertiesConfiguration() {
		Properties prop = new Properties();

		// Properties
		prop.setProperty("mail.pop3.starttls.enable", isStartTLS() ? "true": "false");
		prop.setProperty("mail.pop3.socketFactory.class", getUseSSL());
		prop.setProperty("mail.pop3.socketFactory.fallback", isUseSocketFactoryFallback() ? "true": "false");

		prop.setProperty("mail.pop3.port",getPop3Port() );
		prop.setProperty("mail.pop3.socketFactory.port", getPop3SocketFactory());
		
		return prop;

	}	
}
