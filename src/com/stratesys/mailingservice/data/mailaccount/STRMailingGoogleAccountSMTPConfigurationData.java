package com.stratesys.mailingservice.data.mailaccount;

import java.util.Properties;

/**
 * 
 * @author Diego.
 *
 */
public class STRMailingGoogleAccountSMTPConfigurationData extends STRMailingAccountConfigurationData{


	private static final String GOOGLE_MAIL_HOST = "smtp.gmail.com";
	private static final String GOOGLE_SMTP_PORT = "587";

	public STRMailingGoogleAccountSMTPConfigurationData(String smtpMailUser) throws Exception{
		
		setStartTLS(true);
		setHost("smtp.gmail.com");
		setMailSmtpAuth("true");
		setMailSmtpHost(GOOGLE_MAIL_HOST);
		setMailSmtpPort("587");
		setMailSmtpUser(smtpMailUser);
		setStoreCode("smtp");
	}

	@Override
	public Properties getPropertiesConfiguration() {
		Properties props = new Properties();

		props.put("mail.smtp.host", GOOGLE_MAIL_HOST);
		props.setProperty("mail.smtp.starttls.enable", isStartTLS() ? "true":"false");
		props.setProperty("mail.smtp.port",GOOGLE_SMTP_PORT);
		props.setProperty("mail.smtp.user", getMailSmtpUser());
		props.setProperty("mail.smtp.auth", getMailSmtpAuth());
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.class", "mail.smtp.socketfactory");
	
		return props;

	}	
}
