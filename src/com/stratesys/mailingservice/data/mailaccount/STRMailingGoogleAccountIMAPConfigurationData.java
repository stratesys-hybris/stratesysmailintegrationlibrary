package com.stratesys.mailingservice.data.mailaccount;

import java.util.Properties;

/**
 * 
 * @author Diego.
 *
 */
public class STRMailingGoogleAccountIMAPConfigurationData extends STRMailingAccountConfigurationData{


	private static final String GOOGLE_MAIL_HOST = "imap.googlemail.com";
	private static final String GOOGLE_IMAP_PORT = "993";

	public STRMailingGoogleAccountIMAPConfigurationData() throws Exception{
		  
		setMailImapHost(GOOGLE_MAIL_HOST);
		setMailImapPort(GOOGLE_IMAP_PORT);
		setHost("imap.gmail.com");
		setStoreCode("imap");
	}

	@Override
	public Properties getPropertiesConfiguration() {
		Properties props = new Properties();

//		props.put("mail.imap.host", GOOGLE_MAIL_HOST);
	//	props.put("mail.imap.port", GOOGLE_IMAP_PORT);
	
	
		return props;

	}	
}
