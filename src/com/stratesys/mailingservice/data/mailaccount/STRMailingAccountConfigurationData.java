package com.stratesys.mailingservice.data.mailaccount;

import java.util.Properties;

/**
 * 
 * @author Diego.
 *
 */
public abstract class STRMailingAccountConfigurationData {

	private boolean startTLS = false;
	//SI se usa tls, hay que usar "mail.pop3.socketFactory.class","javax.net.ssl.SSLSocketFactory"
	private String useSSL = "";
	private boolean useSocketFactoryFallback = false;
	private String pop3Port;
	private String pop3SocketFactory;
	private String storeCode;
	private String mailSmtpHost;
	private String mailSmtpPort;
	private String mailSmtpUser;
	private String mailSmtpAuth;
	private String mailImapHost;
	private String mailImapPort;
	private String mailImapUser;
	private String host;

	
	public STRMailingAccountConfigurationData(){};
	/**
	 * creates the account configuration
	 * @param startTLS
	 * @param useSSL
	 * @param useSocketFactoryFallback
	 * @param pop3Port
	 * @param pop3SocketFactory
	 * @param storeCode - The store code for the connection, example pop3
	 * @throws Exception
	 */
	public STRMailingAccountConfigurationData(boolean startTLS, String useSSL, boolean useSocketFactoryFallback, 
			String pop3Port, String pop3SocketFactory, String storeCode) throws Exception{
		if (pop3Port == null || pop3Port.isEmpty())
			throw new Exception("Error creating mail account configurationData, pop3Port cannot be blank");
		if (pop3SocketFactory == null || pop3SocketFactory.isEmpty())
			throw new Exception("Error creating mail account configurationData, pop3SocketFactory cannot be blank");
		if (storeCode == null || storeCode.isEmpty())
			throw new Exception("Error creating mail account configurationData, storeCode cannot be blank");
		
		this.startTLS = startTLS;
		this.useSSL = useSSL;
		this.useSocketFactoryFallback= useSocketFactoryFallback;
		this.pop3Port= pop3Port;
		this.pop3SocketFactory = pop3SocketFactory;
		this.storeCode = storeCode;
	}

	public abstract Properties getPropertiesConfiguration();

	public boolean isStartTLS() {
		return startTLS;
	}


	protected void setStartTLS(boolean startTLS) {
		this.startTLS = startTLS;
	}


	public String getUseSSL() {
		return useSSL;
	}


	protected void setUseSSL(String useSSL) {
		this.useSSL = useSSL;
	}


	public boolean isUseSocketFactoryFallback() {
		return useSocketFactoryFallback;
	}


	protected void setUseSocketFactoryFallback(boolean useSocketFactoryFallback) {
		this.useSocketFactoryFallback = useSocketFactoryFallback;
	}


	public String getPop3Port() {
		return pop3Port;
	}


	protected void setPop3Port(String pop3Port) {
		this.pop3Port = pop3Port;
	}


	public String getPop3SocketFactory() {
		return pop3SocketFactory;
	}


	protected void setPop3SocketFactory(String pop3SocketFactory) {
		this.pop3SocketFactory = pop3SocketFactory;
	}

	public String getStoreCode() {
		return storeCode;
	}

	protected void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getMailSmtpHost() {
		return mailSmtpHost;
	}
	public void setMailSmtpHost(String mailSmtpHost) {
		this.mailSmtpHost = mailSmtpHost;
	}
	public String getMailSmtpPort() {
		return mailSmtpPort;
	}
	public void setMailSmtpPort(String mailSmtpPort) {
		this.mailSmtpPort = mailSmtpPort;
	}
	public String getMailSmtpUser() {
		return mailSmtpUser;
	}
	public void setMailSmtpUser(String mailSmtpUser) {
		this.mailSmtpUser = mailSmtpUser;
	}
	public String getMailSmtpAuth() {
		return mailSmtpAuth;
	}
	public void setMailSmtpAuth(String mailSmtpAuth) {
		this.mailSmtpAuth = mailSmtpAuth;
	}
	public String getMailImapHost() {
		return mailImapHost;
	}
	public void setMailImapHost(String mailImapHost) {
		this.mailImapHost = mailImapHost;
	}
	public String getMailImapPort() {
		return mailImapPort;
	}
	public void setMailImapPort(String mailImapPort) {
		this.mailImapPort = mailImapPort;
	}
	
	public String getMailImapUser() {
		return mailImapUser;
	}
	public void setMailImapUser(String mailImapUser) {
		this.mailImapUser = mailImapUser;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}	
	public String getPort(){
		if (mailImapPort != null)
			return mailImapPort;
		else
			if ( pop3Port != null)
				return pop3Port;
			else
				if (mailSmtpPort != null)
					return mailSmtpPort;
				else
					return null;
	}
	
	public String getUser(){
		if (mailSmtpUser != null)
			return mailSmtpUser;
		else
			if (mailImapUser != null)
				return mailImapUser;
			else
				return null;
	}
}
