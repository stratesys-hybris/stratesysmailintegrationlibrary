package com.stratesys.mailingservice.data.mailaccount;

import java.util.Properties;

/**
 * 
 * @author Diego.
 *
 */
public class STRMailingHDREstrellaAccountIMAPConfigurationData extends STRMailingAccountConfigurationData{


	private static final String HDR_MAIL_HOST = "outlook.office365.com";
	private static final String HDR_IMAP_PORT = "993";

	public STRMailingHDREstrellaAccountIMAPConfigurationData(String imapMailUser) throws Exception{
		  
		
		setMailImapUser(imapMailUser);
		setMailImapHost(HDR_MAIL_HOST);
		setMailImapPort(HDR_IMAP_PORT);
		setHost(HDR_MAIL_HOST);
		setStoreCode("imaps");
	}

	@Override
	public Properties getPropertiesConfiguration() {
		Properties props = new Properties();

		 // Set manual Properties
        props.setProperty("mail.imaps.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.imaps.socketFactory.fallback", "false");
        props.setProperty("mail.imaps.port", "993");
        props.setProperty("mail.imaps.socketFactory.port", "993");
        props.setProperty("mail.imaps.host", "outlook.office365.com");
    	props.setProperty("mail.imaps.ssl.enable","true");
    	props.setProperty("mail.imaps.timeout", "30000");
		
		props.put("mail.imaps.host", HDR_MAIL_HOST);
		props.setProperty("mail.imap.starttls.enable", "true");
		props.setProperty("mail.imaps.user", getMailImapUser());
		props.setProperty("mail.imaps.password", "KDCG7ESF0r");
		
		
	props.put("mail.imaps.auth.plain.disable", true);
	props.put("mail.imaps.auth.ntlm.disable", true);
	
	//props.put("mail.imap.auth.ntlm.disable", false);

	return props;

	}
	
	
}
