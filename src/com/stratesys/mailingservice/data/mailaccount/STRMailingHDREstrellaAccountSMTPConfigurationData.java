package com.stratesys.mailingservice.data.mailaccount;

import java.util.Properties;

/**
 * 
 * @author Diego.
 *
 */
public class STRMailingHDREstrellaAccountSMTPConfigurationData extends STRMailingAccountConfigurationData{


	private static final String HDR_MAIL_HOST = "smtp.office365.com";
	private static final String HDR_SMTP_PORT = "587";

	public STRMailingHDREstrellaAccountSMTPConfigurationData(String smtpMailUser) throws Exception{
		
		setStartTLS(true);
		setHost(HDR_MAIL_HOST);
		setMailSmtpAuth("true");
		setMailSmtpHost(HDR_MAIL_HOST);
		setMailSmtpPort(HDR_SMTP_PORT);
		setMailSmtpUser(smtpMailUser);
		setStoreCode("smtps");
	}

	@Override
	public Properties getPropertiesConfiguration() {
		Properties props = new Properties();

		props.put("mail.smtp.host", HDR_MAIL_HOST);
		props.setProperty("mail.smtp.starttls.enable", isStartTLS() ? "true":"false");
		props.setProperty("mail.smtp.port",HDR_SMTP_PORT);
		props.setProperty("mail.smtp.user", getMailSmtpUser());
		props.setProperty("mail.smtp.auth", getMailSmtpAuth());
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtp.class", "mail.smtp.socketfactory");
		props.put("mail.smtp.timeout", "30000");
	
		return props;

	}	
}
