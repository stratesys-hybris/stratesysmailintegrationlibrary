package com.stratesys.mailingservice.data.mailaccount;

import java.util.Properties;

/**
 * 
 * @author Diego.
 *
 */
public class STRMailingHDREstrellaAccountPOP3ConfigurationData extends STRMailingAccountConfigurationData{


	private static final String HDR_SSL = "javax.net.ssl.SSLSocketFactory";
	private static final String HDR_POP3_PORT = "995";

	public STRMailingHDREstrellaAccountPOP3ConfigurationData() throws Exception{
		
		setStartTLS(false);
		setUseSSL(HDR_SSL);
		setHost("Outlook.office365.com");
		setUseSocketFactoryFallback(false);
		//Para google, por defecto 995
		setPop3Port(HDR_POP3_PORT);
		setPop3SocketFactory(HDR_POP3_PORT);
		setStoreCode("pop3");
	}

	@Override
	public Properties getPropertiesConfiguration() {
		Properties prop = new Properties();

		// Properties
		prop.setProperty("mail.pop3.starttls.enable", isStartTLS() ? "true": "false");
		prop.setProperty("mail.pop3.socketFactory.class", getUseSSL());
		prop.setProperty("mail.pop3.socketFactory.fallback", isUseSocketFactoryFallback() ? "true": "false");

		prop.setProperty("mail.pop3.port",getPop3Port() );
		prop.setProperty("mail.pop3.socketFactory.port", getPop3SocketFactory());
		
		return prop;

	}	
}
