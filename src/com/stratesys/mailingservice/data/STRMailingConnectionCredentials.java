package com.stratesys.mailingservice.data;

public class STRMailingConnectionCredentials {

	private String host;
	private String user;
	private String pass;
	private String folderAlias;
	
	public STRMailingConnectionCredentials(String host, String user, String pass, String folderAlias){
		//Host / User / Pass not null
		assert(host != null && !host.isEmpty());
		assert(user != null && !user.isEmpty());
		assert(pass != null && !pass.isEmpty());
		
		this.host = host;
		this.user = user;
		this.pass = pass;
		this.folderAlias = folderAlias;
	}
	public String getHost() {
		return host;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getPass() {
		return pass;
	}
	public String getFolderAlias() {
		return folderAlias;
	}

	
}
