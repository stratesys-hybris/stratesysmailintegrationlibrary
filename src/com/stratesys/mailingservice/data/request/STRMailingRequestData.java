package com.stratesys.mailingservice.data.request;

import java.util.Date;
import java.util.List;

import com.stratesys.mailingservice.data.STRMailingAttachmentsData;

public class STRMailingRequestData {

	String agentName;
	String fromMail;
	String toMail;
	String replyMessageId;
	
	public String getFromMail() {
		return fromMail;
	}
	public void setFromMail(String fromMail) {
		this.fromMail = fromMail;
	}
	public String getToMail() {
		return toMail;
	}
	public void setToMail(String toMail) {
		this.toMail = toMail;
	}
	String asunto;
	String mensaje;
	List<STRMailingAttachmentsData> ficherosAdjuntos;
	String idThread;
	String cc;
	String commercialMail;
	
	
	
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getCommercialMail() {
		return commercialMail;
	}
	public void setCommercialMail(String commercialMail) {
		this.commercialMail = commercialMail;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<STRMailingAttachmentsData> getFicherosAdjuntos() {
		return ficherosAdjuntos;
	}
	public void setFicherosAdjuntos(List<STRMailingAttachmentsData> ficherosAdjuntos) {
		this.ficherosAdjuntos = ficherosAdjuntos;
	}
	public String getIdThread() {
		return idThread;
	}
	public void setIdThread(String idThread) {
		this.idThread = idThread;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getReplyMessageId() {
		return replyMessageId;
	}
	public void setReplyMessageId(String replyMessageId) {
		this.replyMessageId = replyMessageId;
	}
	
	
}
