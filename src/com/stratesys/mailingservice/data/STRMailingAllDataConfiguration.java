package com.stratesys.mailingservice.data;

import javax.mail.Folder;
import javax.mail.Session;
import javax.mail.Transport;

public class STRMailingAllDataConfiguration {

	Session session = null;
	Folder imapFolder = null;
	Transport transport = null;
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	public Folder getImapFolder() {
		return imapFolder;
	}
	public void setImapFolder(Folder imapFolder) {
		this.imapFolder = imapFolder;
	}
	public Transport getTransport() {
		return transport;
	}
	public void setTransport(Transport transport) {
		this.transport = transport;
	}
	
}
