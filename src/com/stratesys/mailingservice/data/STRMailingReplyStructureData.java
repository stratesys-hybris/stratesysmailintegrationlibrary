package com.stratesys.mailingservice.data;

import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Locale;

public class STRMailingReplyStructureData {

	final String PATTERN_MAIL = "#mail#";
	final String PATTERN_DATESENT = "#datesent#";
	final String PATTERN_SUBJECT = "#subject#";
	
	String from = "De: [#mail#]";
	String sentWhen = "Enviado el: #dateSent#";
	String to = "Para :[#mail#]";
	String subject = "Asunto: #subject#";
	String message = "";
	
	
	public STRMailingReplyStructureData(String from ,Date sentWhen, String to, String subject, String message){
		
		this.from = this.from.replaceAll(PATTERN_MAIL, from);
		
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, new Locale ( "es" , "ES" ));
		String dateFormatted = dateFormatter.format(sentWhen);
		this.sentWhen.replaceAll(PATTERN_DATESENT, dateFormatted);
		this.to = this.to.replaceAll(PATTERN_MAIL, to);
		
		this.subject.replaceAll(PATTERN_SUBJECT, subject);
		this.message = message;		
	}
	
	public String getDataPrepared(){
		return from  + "/n" + sentWhen + "/n" + to + "/n" + subject  + "/n/n" + message; 
	}
}
