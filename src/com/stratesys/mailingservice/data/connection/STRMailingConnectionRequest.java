package com.stratesys.mailingservice.data.connection;

import javax.mail.Folder;

public class STRMailingConnectionRequest {

	boolean developmentMode = false;
	String folderName = "INBOX";
	int connectionMode = Folder.READ_ONLY;

	
	public STRMailingConnectionRequest (boolean developmentMode, String folderName, int connectionMode){
		this.developmentMode = developmentMode;
		this.folderName = folderName;
		this.connectionMode = connectionMode;
		
	}

	public boolean isDevelopmentMode() {
		return developmentMode;
	}

	public void setDevelopmentMode(boolean developmentMode) {
		this.developmentMode = developmentMode;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public int getConnectionMode() {
		return connectionMode;
	}

	public void setConnectionMode(int connectionMode) {
		this.connectionMode = connectionMode;
	}

}
