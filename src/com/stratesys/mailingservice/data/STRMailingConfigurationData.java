package com.stratesys.mailingservice.data;

import com.stratesys.mailingservice.data.mailaccount.STRMailingAccountConfigurationData;

public class STRMailingConfigurationData {

	private STRMailingAccountConfigurationData mailAccountConfiguration;
	private STRMailingConnectionCredentials credentials;
	
	public STRMailingConfigurationData(STRMailingAccountConfigurationData mailAccountConfiguration, STRMailingConnectionCredentials credentials){
		this.mailAccountConfiguration = mailAccountConfiguration;
		this.credentials = credentials;
	}
	

	public STRMailingAccountConfigurationData getMailAccountConfiguration() {
		return mailAccountConfiguration;
	}

	public void setMailAccountConfiguration(STRMailingAccountConfigurationData mailAccountConfiguration) {
		this.mailAccountConfiguration = mailAccountConfiguration;
	}

	public STRMailingConnectionCredentials getCredentials() {
		return credentials;
	}

	public void setCredentials(STRMailingConnectionCredentials credentials) {
		this.credentials = credentials;
	}
	
}
