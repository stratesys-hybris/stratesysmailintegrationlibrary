package com.stratesys.mailingservice.enumerations;

public enum STRMailingAccountTypes {
	GOOGLE_POP, GOOGLE_SMTP
}
