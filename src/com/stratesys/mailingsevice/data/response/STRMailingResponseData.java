package com.stratesys.mailingsevice.data.response;

import java.util.Date;
import java.util.List;

import com.stratesys.mailingservice.data.STRMailingAttachmentsData;

public class STRMailingResponseData {

	Date fecha;
	String remitente;
	String asunto;
	String mensaje;
	List<STRMailingAttachmentsData> ficherosAdjuntos;
	String idThread;
	String originalThreadId;
	String mailReceivedFrom;
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getRemitente() {
		return remitente;
	}
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<STRMailingAttachmentsData> getFicherosAdjuntos() {
		return ficherosAdjuntos;
	}
	public void setFicherosAdjuntos(List<STRMailingAttachmentsData> ficherosAdjuntos) {
		this.ficherosAdjuntos = ficherosAdjuntos;
	}
	public String getIdThread() {
		return idThread;
	}
	public void setIdThread(String idThread) {
		this.idThread = idThread;
	}
	public String getOriginalThreadId() {
		return originalThreadId;
	}
	public void setOriginalThreadId(String originalThreadId) {
		this.originalThreadId = originalThreadId;
	}
	public String getMailReceivedFrom() {
		return mailReceivedFrom;
	}
	public void setMailReceivedFrom(String mailReceivedFrom) {
		this.mailReceivedFrom = mailReceivedFrom;
	}
	
	
}
